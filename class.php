
<?php

class Htmalpage {
        protected $title = "example title";
        protected $body = "example body";
        
        public function view(){
            echo "<html><head><title>$this->title</title></head><body>$this->body</body></html>";
        }

        function __construct($title="",$body=""){
            if ($title != ""){
                $this->title = $title;
            }
            if ($body !=""){
                $this->body = $body;
            }
        }
    }   
    
    class coloredMessage extends Htmalpage{
        protected $colors = array ('red','yellow','green');
        protected $color ='red';
        public function __set($property,$value){
        if($property == 'color'){
            if (in_array($value,$this->colors)){
                $this->color = $value;
        }
        else{
            die("error");
        }
    }
}

    public function show(){
        echo "<html><head><title>$this->title</title></head><body><p style='color: $this->color'>$this->body</p></body></html>";
    }
}

    class sizeMessage extends coloredMessage{
        protected $size;
        public function __set($property,$value){
            if ($property == 'color'){
            parent::__set($property, $value);
            }
            if($property == 'size'){
                $sizes = array('10','11','12','13','14','15','16','17','18','19','20','21','22','23','24');
                if (in_array($value,$sizes)){
                    $this->size = $value;
                }
                else{
                    die("wrong size");
                }
            }
        }
        public function show(){
            echo "<html><head><title>$this->title</title></head><body><p style = 'font-size:$this->size; color:$this->color'>$this->body</p></body></html>";
        }
    }

    function showObject($object){
        $object->show();
    }
?>